#-----Statement of Authorship----------------------------------------#
#
#  By submitting this task the signatories below agree that it
#  represents our own work and that we both contributed to it.  We
#  are aware of the University rule that a student must not
#  act in a manner which constitutes academic dishonesty as stated
#  and explained in QUT's Manual of Policies and Procedures,
#  Section C/5.3 "Academic Integrity" and Section E/2.1 "Student
#  Code of Conduct".
#
#  First student's no: n8824088
#  First student's name: Scott J. Schultz
#  Portfolio contribution: 100%
#
#  Second student's no: SOLO SUBMISSION
#
#  Contribution percentages refer to the whole portfolio, not just this
#  task.  Percentage contributions should sum to 100%.  A 50/50 split is
#  NOT necessarily expected.  The percentages will not affect your marks
#  except in EXTREME cases.
#
#--------------------------------------------------------------------#


#-----Task Description-----------------------------------------------#
#
#  DIGITAL GRAFFITI
#
#  In this task you develop two functions for changing and restoring
#  image files.  You need to develop two functions:
#
#  1. vandalise - Given an artwork file and a graffito file,
#     overwrite the graffito onto the artwork.  (BTW, "graffito" is
#     the singular of graffiti!)
#
#  2. restore - Given an artwork file that has been vandalised with
#     a graffito, restore the artwork to as close to its original
#     condition as possible.
#
#  To do this you will need to make use of the functions provided
#  by the Python Imaging Library (PIL).  See the task's instructions
#  for further detail.
#
#--------------------------------------------------------------------#


#-----Automatic Tests------------------------------------------------#
#
#  This section contains unit 'tests' that are used to call your
#  functions.  Note that "passing" these tests does NOT mean that you
#  have satisfied the requirements for the task, because these tests
#  merely call the functions, but do not check that the image files
#  produced are correct.
#
#  NB: Running these 'tests' will delete old copies of vandalised
#  ('*_X.bmp') and restored ('*_X_O.bmp') files, if any, in the
#  current directory.

"""
Test 1. Vandalise Da Vinci's Mona Lisa
>>> try:
...     remove('MonaLisa_X.bmp')
... except:
...     pass
>>> vandalise('MonaLisa.bmp', 'Graffito.bmp')
True

Test 2. Restore Da Vinci's Mona Lisa
>>> try:
...     remove('MonaLisa_X_O.bmp')
... except:
...     pass
>>> restore('MonaLisa_X.bmp')
True

Test 3. Vandalise Van Gogh's Sunflowers
>>> try:
...     remove('Sunflowers_X.bmp')
... except:
...     pass
>>> vandalise('Sunflowers.bmp', 'Graffito.bmp')
True

Test 4. Restore Van Gogh's Sunflowers
>>> try:
...     remove('Sunflowers_X_O.bmp')
... except:
...     pass
>>> restore('Sunflowers_X.bmp')
True

Test 5. Vandalise Turner's Flint Castle
>>> try:
...     remove('FlintCastle_X.bmp')
... except:
...     pass
>>> vandalise('FlintCastle.bmp', 'Graffito.bmp')
True

Test 6. Restore Turner's Flint Castle
>>> try:
...     remove('FlintCastle_X_O.bmp')
... except:
...     pass
>>> restore('FlintCastle_X.bmp')
True

Test 7. Try and fail to vandalise a non-existent painting
>>> vandalise('TheScream.bmp', 'Graffito.bmp')
False

Test 8. Try and fail to restore a non-existent painting
>>> restore('NudeDescendingStaircase_X.bmp')
False

"""
# 
#--------------------------------------------------------------------#

#-----Students' Solution---------------------------------------------#
#
#  Complete the task by filling in the template below.

#------------------------------------------------------------------------------#
#                                                                              #
#                                   IMPORTS                                    #
#                                                                              #
#------------------------------------------------------------------------------#

from PIL import Image
import os

#------------------------------------------------------------------------------#
#                                                                              #
#                                   GLOBALS                                    #
#                                                                              #
#------------------------------------------------------------------------------#



#------------------------------------------------------------------------------#
#                                                                              #
#                                   CONSTANTS                                  #
#                                                                              #
#------------------------------------------------------------------------------#


##### PUT YOUR vandalise AND restore FUNCTIONS HERE

def load_image(filename,conversion_mode):
	"""
	Checks to see if a given image, filename, exists in the current directory of
	the script.

	If it does, the function converts the image to a specified mode eg:
	RGB,L,1,RGBA and other modes supported by PIL; then returns the image object.

	Otherwise, False is returned and the parent function will most likely also
	return False, as it has no image to work with.
	"""
	try:
		image = Image.open(filename)
	except:
		return False
	else:
		image = image.convert(conversion_mode)
		return image

def remove(filename):
	"""
	Removes a given file, filename. Throws no error on fail.

	Instead, if the given file isn't found the function wipes
	the	whole directory 
	"""
	try:
		os.remove(filename)
	except:
		pass

def crop_and_overlay(original_image,graffito):
	"""
	Gets the image dimensions of the original image, as well as the graffito image.

	Using these values the function calculates offsets, crops a region of size
	graffito out of original_image.

	The graffito image is then looped through, and where a black pixel is reached,
	the corresponding pixel in the cropped original image is replaced with black.

	The updated pixel data for the cropped data is then put back into the original
	cropped image, and is then returned along with the region coordinates for the
	cropped area
	"""
	original_xsize, original_ysize = original_image.size
	graffito_xsize, graffito_ysize = graffito.size


	top_offset = graffito_ysize - (graffito_ysize/2)
	top_offset = original_ysize/2 - top_offset

	left_offset = graffito_xsize - (graffito_xsize/2)
	left_offset = original_xsize/2 - left_offset

	crop_box = (left_offset,top_offset,(left_offset+graffito_xsize),(top_offset+graffito_ysize))

	cropped = original_image.crop(crop_box)

	graffito_pixel_data = graffito.getdata()
	cropped_pixel_data = list(cropped.getdata())

	for i, pixel in enumerate(graffito_pixel_data):
		if pixel == 0:
			cropped_pixel_data[i] = (0,0,0,255)
	cropped.putdata(cropped_pixel_data)
	return crop_box,cropped

def vandalise(original_image_filename,graffito_filename):
	"""

	Attempts to destroy priceless, one of a kind artwork given an image,
	and a black and white image to destroy it with.

	
	"""
	original_image = load_image(original_image_filename,"RGB")
	graffito = load_image(graffito_filename,"1")

	if original_image and graffito:

		crop_data = crop_and_overlay(original_image,graffito)

		# Given the return from the crop_and_overlay function, pastes the portion
		# of the original image that was cropped and defaced in the same location
		# that the image was cropped from.
		original_image.paste(crop_data[1],crop_data[0])

		new_filename = "{filename}_X.{extension}".format(
			filename = original_image_filename.split('.')[0],
			extension = original_image_filename.split('.')[1]
		)
		original_image.save(new_filename)
		return True
	else:
		return False

def restore(defaced_image_filename):
	"""
	
	Attempts to restore an image defaced with black pixels.

	Ironically this will also remove black pixels in general.

	That's not really irony.

	"""
	defaced_image = load_image(defaced_image_filename,"RGB")

	if not defaced_image:
		return False

	defaced_image_xsize, defaced_image_ysize = defaced_image.size

	defaced_image_pixel_data = list(defaced_image.getdata())

	restored_pixel_data = []

	for i, pixel in enumerate(defaced_image_pixel_data):
		if pixel == (0,0,0):
			top_pixel_data = defaced_image_pixel_data[i-defaced_image_xsize]

			temp_counter_i = i+1
			while (defaced_image_pixel_data[temp_counter_i] == (0,0,0)):
				temp_counter_i += 1
			right_pixel_data = defaced_image_pixel_data[temp_counter_i]

			temp_counter_i = i+defaced_image_xsize
			while (defaced_image_pixel_data[temp_counter_i] == (0,0,0)):
				temp_counter_i += defaced_image_xsize
			bottom_pixel_data = defaced_image_pixel_data[temp_counter_i]

			left_pixel_data = defaced_image_pixel_data[i-1]

			new_pixel_data = (
				(top_pixel_data[0]+right_pixel_data[0]+bottom_pixel_data[0]+left_pixel_data[0])/4,
				(top_pixel_data[1]+right_pixel_data[1]+bottom_pixel_data[1]+left_pixel_data[1])/4,
				(top_pixel_data[2]+right_pixel_data[2]+bottom_pixel_data[2]+left_pixel_data[2])/4
			)
			defaced_image_pixel_data[i] = new_pixel_data
	defaced_image.putdata(defaced_image_pixel_data)

	new_filename = "{filename}_O.{extension}".format(
		filename = defaced_image_filename.split('.')[0],
		extension = defaced_image_filename.split('.')[1]
	)
	defaced_image.save(new_filename)
	return True

#
#--------------------------------------------------------------------#



#-----Testing--------------------------------------------------------#
#
# The following code (when uncommented) will run your functions on
# the supplied images.  You should comment it out while developing
# your functions, but leave it uncommented when you submit your
# solution for marking.
#

if __name__ == '__main__':
	from os import remove
	from doctest import testmod
	testmod(verbose=True)

#
#--------------------------------------------------------------------#
